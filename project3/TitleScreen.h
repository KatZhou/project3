//
//  TitleScreen.h
//  project3
//
//  Created by Kat Zhou on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// TitleScreen
@interface TitleScreen : CCLayer
{
    CCSpriteBatchNode *batch;
    CGSize screen;
    int highScore;
}

// returns a CCScene that contains the TitleScreen as the only child
+(CCScene *) scene;

@end
