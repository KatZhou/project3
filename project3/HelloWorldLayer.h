//
//  HelloWorldLayer.h
//  project3
//
//  Created by Kat Zhou on 4/9/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "CCParallaxScrollNode.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
    CCParallaxScrollNode *background;
    CGSize screen;
    CCSpriteBatchNode *batch;
    CCSprite *malan;
    CCSprite *bg1a;
    CCSprite *bg1b;
    CCSprite *bg2a;
    CCSprite *bg2b;
    CCSprite *bg3a;
    CCSprite *bg3b;
    CCArray *lasers;
    CCArray *blocks;
    CCArray *bugs;
    CCLabelAtlas *scoreLabel;
    int laserNum;
    int blockNum;
    int bugNum;
    int totalLasers;
    int totalBlocks;
    int totalBugs;
    float blocksOnScreen;
    float bugsOnScreen;
    float moveSpeed;
    bool isTouched;
    float touchTime;
    int score;
    int highScore;
    float currTime;
    float nextBlockTime;
    float nextBugTime;
    float blockUpperBound;
    float bugUpperBound;
    bool isGameOver;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

//returns a random value between low and high
-(float) randVal:(float)low high:(float)high;

//removes a laser
- (void)removeLaser:(CCSprite *)laser;

//removes a block
- (void)removeBlock:(CCSprite *)block;

//removes a bug
- (void)removeBug:(CCSprite *)bug;

//loses the game
- (void)loseGame;

@end

