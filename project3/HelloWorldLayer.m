//
//  HelloWorldLayer.m
//  project3
//
//  Created by Kat Zhou on 4/9/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "CCParallaxScrollNode.h"
#import "CCTouchDispatcher.h"
#import "SimpleAudioEngine.h"
#import "TitleScreen.h"

// HelloWorldLayer implementation
@implementation HelloWorldLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    if((self=[super init])) 
    {        
        //enables touches
        self.isTouchEnabled = YES;
        
        //don't display FPS
        [[CCDirector sharedDirector] setDisplayFPS:NO]; 
        
        //plays music
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"supermalan.m4a"];
        
        //initialize laserNum, blockNum, bugNum, numBlocksOnScreen, numBugsOnScreen
        laserNum = 0;
        blockNum = 0;
        bugNum = 0;
        blocksOnScreen = 0;
        bugsOnScreen = 0;
        
        //initialize currTime to 0
        currTime = 0;
        
        //initialize isGameOver to NO
        isGameOver = NO;
        
        // set default highScore
        NSMutableDictionary *defaultValues = [[NSMutableDictionary alloc] init];
        [defaultValues setObject:[NSNumber numberWithInt:0] forKey:@"highScore"];
        
        // register defaults
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults registerDefaults:defaultValues];
        
        // load saved info  
        highScore= [[defaults objectForKey:@"highScore"] intValue];
        
        //initialize score to 0
        score = 0;
        scoreLabel = [CCLabelAtlas labelWithString:@"0" charMapFile:@"fps_images.png" itemWidth:16 itemHeight:24 startCharMap:'.'];
        [scoreLabel setString:@"0"];
        scoreLabel.position = ccp(10, 10);
        [self addChild:scoreLabel z:3];
        [scoreLabel draw];
        
        //sets random times for next block and bug to appear
        blockUpperBound = 5.0;
        bugUpperBound = 5.0;
        nextBlockTime = [self randVal:0.0 high:3.0];
        nextBugTime = [self randVal:1.0 high: 2.0];
        
        //set settings for batch node
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
        batch = [CCSpriteBatchNode batchNodeWithFile:@"sprites.pvr.ccz"];
        [self addChild:batch];    
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprites.plist"];
        
        //create Malan sprite
        malan = [CCSprite spriteWithSpriteFrameName:@"malan.png"];
        
        //obtain device size
        screen = [CCDirector sharedDirector].winSize;
        
        //resize Malan
        malan.scale = 0.8;

        //position Malan
        malan.position = ccp(screen.width * 0.15, screen.height * 0.5);
        
        //display Malan
        [batch addChild:malan z:2];
        
        //adds background as parallax node
        background = [[[CCParallaxScrollNode alloc] init] retain];
        
        //create background images and add them to the background
        bg1a = [CCSprite spriteWithSpriteFrameName:@"bg1.png"];
        bg1b = [CCSprite spriteWithSpriteFrameName:@"bg1.png"];
        [background addInfiniteScrollXWithZ:0 Ratio:ccp(0.5,0.5) Pos:CGPointZero Objects:bg1a, bg1b, nil];
        
        bg2a = [CCSprite spriteWithSpriteFrameName:@"bg2.png"];
        bg2b = [CCSprite spriteWithSpriteFrameName:@"bg2.png"];
        [background addInfiniteScrollXWithZ:0 Ratio:ccp(1,1) Pos:CGPointZero Objects:bg2a, bg2b, nil];
        
        bg3a = [CCSprite spriteWithSpriteFrameName:@"bg3.png"];
        bg3b = [CCSprite spriteWithSpriteFrameName:@"bg3.png"];
        [background addInfiniteScrollXWithZ:0 Ratio:ccp(2,2) Pos:CGPointZero Objects:bg3a, bg3b, nil];

        [self addChild:background z:-1];
        
        //allows for 10 lasers to be fired at a time
        CCSprite *oneLaser;
        totalLasers = 10;
        lasers = [[CCArray alloc] initWithCapacity:totalLasers];
        for(int i = 0; i < totalLasers; i++) 
        {
            oneLaser = [CCSprite spriteWithSpriteFrameName:@"laser.png"];
            oneLaser.visible = NO;
            [batch addChild:oneLaser z:1];
            [lasers addObject:oneLaser];
        }
        
        //allows for 5 blocks to be onscreen at a time
        CCSprite *oneBlock;
        totalBlocks = 5;
        blocks = [[CCArray alloc] initWithCapacity:totalBlocks];
        for(int i = 0; i < totalBlocks; i++) 
        {
            oneBlock = [CCSprite spriteWithSpriteFrameName:@"block.png"];
            oneBlock.visible = NO;
            [batch addChild:oneBlock];
            [blocks addObject:oneBlock];
        }
        
        //allows for 20 bugs to be onscreen at a time
        CCSprite *oneBug;
        totalBugs = 20;
        bugs = [[CCArray alloc] initWithCapacity:totalBugs];
        for(int i = 0; i < (totalBugs/4); i++) 
        {
            oneBug = [CCSprite spriteWithSpriteFrameName:@"bug1.png"];
            oneBug.scale = 2.0;
            oneBug.visible = NO;
            [batch addChild:oneBug];
            [bugs addObject:oneBug];
            oneBug = [CCSprite spriteWithSpriteFrameName:@"bug2.png"];
            oneBug.scale = 2.0;
            oneBug.visible = NO;
            [batch addChild:oneBug];
            [bugs addObject:oneBug];
            oneBug = [CCSprite spriteWithSpriteFrameName:@"bug3.png"];
            oneBug.scale = 2.0;
            oneBug.visible = NO;
            [batch addChild:oneBug];
            [bugs addObject:oneBug];
            oneBug = [CCSprite spriteWithSpriteFrameName:@"bug4.png"];
            oneBug.scale = 2.0;
            oneBug.visible = NO;
            [batch addChild:oneBug];
            [bugs addObject:oneBug];
        }
        
        [self schedule:@selector(update:)];
    }
    return self;
}

// updates the frame with the times
- (void) update:(ccTime)dt
{    
    //current time
    currTime += dt;
    
    //decrease upper bounds after 30 seconds to make the game harder
    if (currTime >= 30 && currTime < 60)
    {
        blockUpperBound = 4.0;
        bugUpperBound = 4.0;
    }
    //decreases upper bounds more after a minute
    if (currTime >= 60 && currTime < 120)
    {
        blockUpperBound = 3.0;
        bugUpperBound = 3.0;
    }
    //makes more bugs appear after two minutes
    if (currTime >= 120 && currTime < 240)
        bugUpperBound = 2.0;
    if (currTime >= 240)
        bugUpperBound = 1.0;
    
    //increase score and update label
    score += 1;
    NSMutableString *scoreStr = [[NSMutableString alloc] initWithFormat:@"%d", score];
    [scoreLabel setString:scoreStr];
    [scoreStr release];
    [scoreLabel draw];
    
    //moves the background
    [background updateWithVelocity:ccp(-3.0, 0) AndDelta:dt];
    
    //moves Malan downwards
    float curr;
    if (isTouched == NO)
    {
        curr =  malan.position.y - (screen.height * 0.3 * dt);
        malan.position = ccp(malan.position.x, curr);
    }
    else
    {
        touchTime += dt;
        if (touchTime > 0.15)
        {
            curr = malan.position.y + (screen.height * 0.3 * dt);
            malan.position = ccp(malan.position.x, curr);
        }
    }
    
    if (malan.position.y >= screen.height)
        [self loseGame];
    
    if (malan.position.y <= 0)
        [self loseGame];
    
    //adds a new block if the time to do so is here
    if (currTime > nextBlockTime && blocksOnScreen < totalBlocks) 
    {        
        float rand = [self randVal:1.0 high:blockUpperBound];
        nextBlockTime = currTime + rand;
        
        float position = [self randVal:0.0 high:screen.height];
        
        CCSprite *block = [blocks objectAtIndex:blockNum];
        blockNum++;
        if (blockNum >= totalBlocks) 
            blockNum = 0;
        
        block.position = ccp(screen.width+block.contentSize.width/2, position);
        block.visible = YES;
        blocksOnScreen++;
        id appear = [CCMoveTo actionWithDuration:7.0 position:ccp(-screen.width - block.contentSize.width, position)];
        id finish = [CCCallFuncN actionWithTarget:self selector:@selector(removeBlock:)];
        [block runAction:[CCSequence actions:appear,finish,nil]];
    }
    
    //adds a new bug if the time to do so is here
    if (currTime > nextBugTime && bugsOnScreen < totalBugs) 
    {        
        float rand = [self randVal:0.0 high:bugUpperBound];
        nextBugTime = currTime + rand;
        
        float position = [self randVal:0.0 high:screen.height];
        float endPos = [self randVal:0.0 high:screen.height];
        
        CCSprite *bug = [bugs objectAtIndex:bugNum];
        bugNum++;
        if (bugNum >= totalBugs) 
            bugNum = 0;
        
        bug.position = ccp(screen.width+bug.contentSize.width/2, position);
        bug.visible = YES;
        bugsOnScreen++;
        id appear = [CCMoveTo actionWithDuration:7.0 position:ccp(-screen.width-bug.contentSize.width, endPos)];
        id finish = [CCCallFuncN actionWithTarget:self selector:@selector(removeBug:)];
        [bug runAction:[CCSequence actions:appear,finish,nil]];
    }
    
    //tests to see if a laser or Malan hit a bug
    for (CCSprite *bug in bugs) 
    {        
        if (bug.visible == NO) 
            continue;
        
        for (CCSprite *laser in lasers) 
        {                        
            if (laser.visible == NO) 
                continue;
            
            if (CGRectIntersectsRect(laser.boundingBox, bug.boundingBox)) 
            {                
                [self removeLaser:laser];
                [self removeBug:bug];  
                score += 20;
                continue;
            }
        }
        
        if (bug.visible == YES && CGRectIntersectsRect(malan.boundingBox, bug.boundingBox)) 
            [self loseGame];
    }
    
    //tests to see if Malan hit a block
    for (CCSprite *block in blocks)
    {
        if (block.visible == YES && CGRectIntersectsRect(block.boundingBox, malan.boundingBox))
            [self loseGame];
    }
}

//returns a random value between two floats
- (float) randVal:(float)low high:(float)high 
{
    return (((float)arc4random() / 0x100000000 * (high - low)) + low);
}

//fired when a touch begins
-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    isTouched = YES;
    touchTime = 0.0;
    return YES;
}

//fired when a touch ends
-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    isTouched = NO;
    
    if (touchTime <= 0.15 && isGameOver == NO)
    {
        //play laser noise
        [[SimpleAudioEngine sharedEngine] playEffect:@"laser.wav"];
        
        //fire laser
        CCSprite *currLaser = [lasers objectAtIndex:laserNum];
        laserNum++;
        if (laserNum >= totalLasers) 
            laserNum = 0;
        
        currLaser.position = ccpAdd(malan.position, ccp(currLaser.contentSize.width/2, 0));
        currLaser.visible = YES;
        id shoot = [CCMoveTo actionWithDuration:1.0 position:ccp(screen.width + 10, currLaser.position.y)];
        id finish = [CCCallFuncN actionWithTarget:self selector:@selector(removeLaser:)];
        [currLaser runAction:[CCSequence actions:shoot,finish,nil]];
        
    }
}

//removes the currect laser
- (void)removeLaser:(CCSprite *)laser 
{
    laser.visible = NO;
}

//removes the current block
- (void)removeBlock:(CCSprite *)block 
{
    block.visible = NO;
    blocksOnScreen--;
}

//removes the current bug
- (void)removeBug:(CCSprite *)bug 
{
    bug.visible = NO;
    bugsOnScreen--;
}

//shows labels with the final score and an option to start over
- (void)loseGame
{
    //pauses scheduled updates
    [self pauseSchedulerAndActions];
    
    //pauses moving sprites
    CCNode *child;
    CCARRAY_FOREACH(batch.children, child) 
    {
        [child stopAllActions];
    }
    
    //stops the ability to shoot lasers
    isGameOver = YES;
    
    //saves high score
    if (score > highScore)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:score] forKey:@"highScore"];
        [defaults synchronize];
        highScore = score;
    }
    
    CCLabelBMFont *gameOverLabel = [CCLabelBMFont labelWithString:@"Nooooooo!" fntFile:@"GameOver.fnt"];
    NSString* endScore =[[NSString alloc]initWithFormat:@"Score: %i",score];
    NSString* highScoreStr =[[NSString alloc]initWithFormat:@"High Score: %i",highScore];
    CCLabelBMFont *endScoreLabel = [CCLabelBMFont labelWithString:endScore fntFile:@"GameOver.fnt"];
    CCLabelBMFont *highScoreLabel = [CCLabelBMFont labelWithString:highScoreStr fntFile:@"GameOver.fnt"];
    CCLabelBMFont *tryAgainLabel = [CCLabelBMFont labelWithString:@"Try Again" fntFile:@"TryAgain.fnt"];
    CCLabelBMFont *mainscreenLabel = [CCLabelBMFont labelWithString:@"Main Menu" fntFile:@"TryAgain.fnt"];
    gameOverLabel.position = ccp(screen.width * 0.5, screen.height * 0.7);
    endScoreLabel.position = ccp(screen.width * 0.5, screen.height * 0.6);
    highScoreLabel.position = ccp(screen.width * 0.5, screen.height * 0.5);
    CCMenuItemLabel *tryAgain = [CCMenuItemLabel itemWithLabel:tryAgainLabel target:self selector:@selector(restart:)];
    tryAgain.position = ccp(screen.width * 0.5, screen.height * 0.4);
    CCMenuItemLabel *mainscreen = [CCMenuItemLabel itemWithLabel:mainscreenLabel target:self selector:@selector(toMain:)];
    mainscreen.position = ccp(screen.width * 0.5, screen.height * 0.3);
    
    CCMenu *gameOver = [CCMenu menuWithItems:tryAgain, mainscreen, nil];
    gameOver.position = CGPointZero;
    [self addChild:gameOver];

    [tryAgain runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [mainscreen runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    
    [self addChild:gameOverLabel];
    [self addChild:endScoreLabel];
    [self addChild:highScoreLabel];
    
}

// Restarts the game
- (void)restart:(id)sender 
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.3 scene:[HelloWorldLayer scene]]];   
}

// Goes to mainscreen
- (void)toMain:(id)sender 
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[TitleScreen scene]]];   
}

// enables touches
- (void)registerWithTouchDispatcher
{
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

// on "dealloc" you need to release all your retained objects
- (void)dealloc
{
    [background release];
	[super dealloc];
}
@end
