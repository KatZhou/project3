//
//  TitleScreen.m
//  project3
//
//  Created by Kat Zhou on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// Import the interfaces
#import "TitleScreen.h"
#import "CCTouchDispatcher.h"
#import "SimpleAudioEngine.h"
#import "HelloWorldLayer.h"

// TitleScreen implementation
@implementation TitleScreen

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TitleScreen *layer = [TitleScreen node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    if((self=[super init])) 
    {        
        //enables touches
        self.isTouchEnabled = YES;
        
        //don't display FPS
        [[CCDirector sharedDirector] setDisplayFPS:NO]; 
        
        //plays music
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"superbass.m4a"];
        
        //obtain device size
        screen = [CCDirector sharedDirector].winSize;
        
        // set default highScore
        NSMutableDictionary *defaultValues = [[NSMutableDictionary alloc] init];
        [defaultValues setObject:[NSNumber numberWithInt:0] forKey:@"highScore"];
        
        // register defaults
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults registerDefaults:defaultValues];
        
        // load saved info  
        highScore= [[defaults objectForKey:@"highScore"] intValue];
        
        //set settings for batch node
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
        batch = [CCSpriteBatchNode batchNodeWithFile:@"sprites.pvr.ccz"];
        [self addChild:batch];    
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprites.plist"];
        
        //create background images and add them to the background
        CCSprite *background = [CCSprite spriteWithSpriteFrameName:@"mainscreen.png"];
        background.position = ccp(screen.width * 0.5, screen.height * 0.5);
        [batch addChild:background z:-1];
        
        CCLabelBMFont *playLabel = [CCLabelBMFont labelWithString:@"PLAY" fntFile:@"MainMenu.fnt"];
        CCMenuItemLabel *play = [CCMenuItemLabel itemWithLabel:playLabel target:self selector:@selector(newGame:)];
        play.position = ccp(screen.width * 0.35, screen.height * 0.3);
        CCLabelBMFont *instructionsLabel = [CCLabelBMFont labelWithString:@"INSTRUCTIONS" fntFile:@"MainMenu.fnt"];
        CCMenuItemLabel *instructions = [CCMenuItemLabel itemWithLabel:instructionsLabel target:self selector:@selector(showInstr:)];
        instructions.position = ccp(screen.width * 0.35, screen.height * 0.2);
        NSString* highScoreStr =[[NSString alloc]initWithFormat:@"High Score: %i",highScore];
        CCLabelBMFont *highScoreLabel = [CCLabelBMFont labelWithString:highScoreStr fntFile:@"TryAgain.fnt"];
        highScoreLabel.position = ccp(screen.width * 0.35, screen.height * 0.1);
        [self addChild:highScoreLabel];
        
        CCMenu *menu = [CCMenu menuWithItems:play, instructions, nil];
        menu.tag = 99;
        menu.position = CGPointZero;
        [self addChild:menu z:0];
        
        [play runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [instructions runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    }
    return self;
}

// Starts a new game
- (void)newGame:(id)sender 
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.3 scene:[HelloWorldLayer scene]]];   
}

// Goes back to the title screen
- (void)toMain:(id)sender 
{
    [self removeChildByTag:100 cleanup:YES];
    [self removeChildByTag:101 cleanup:YES];
}

// Shows the instructions
- (void)showInstr:(id)sender
{
    //CCMenu *menu = 
    CCSprite *instr = [CCSprite spriteWithSpriteFrameName:@"instructions.png"];
    instr.position = ccp(screen.width * 0.5, screen.height * 0.5);
    instr.tag = 100;
    [self addChild:instr z:1];
    
    CCLabelBMFont *backLabel = [CCLabelBMFont labelWithString:@"BACK" fntFile:@"TryAgain.fnt"];
    CCMenuItemLabel *back = [CCMenuItemLabel itemWithLabel:backLabel target:self selector:@selector(toMain:)];
    back.position = ccp(screen.width * 0.9, screen.height * 0.9);
        
    CCMenu *menu = [CCMenu menuWithItems:back, nil];
    menu.position = CGPointZero;
    menu.tag = 101;
    [self addChild:menu z:2];
    
    [back runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];

}


@end
